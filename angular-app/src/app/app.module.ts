import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule, LOCALE_ID } from '@angular/core'
import { registerLocaleData } from '@angular/common'
import ptBr from '@angular/common/locales/pt'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { HeaderComponent } from './components/header/header.component'
import { ListComponent } from './components/citiesList/list/list.component'
import { FilterComponent } from './components/citiesList/filter/filter.component'
import { ListHeaderComponent } from './components/citiesList/list-header/list-header.component'
import { FooterComponent } from './components/footer/footer.component'
import { FormsModule } from '@angular/forms'
import { ApiService } from './services/api.service'
import { CommonModule } from '@angular/common'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { NgbPaginationModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap'
import { HttpRequestInterceptor } from './services/interceptors/http-interceptor.module'
import { ToastNoAnimationModule, ToastrModule } from 'ngx-toastr'
import { LoaderInterceptorService } from './services/interceptors/loader.interceptor'
import { LoaderComponent } from './components/loader/loader.component'
import { TokenInterceptor } from './services/interceptors/token.interceptor'
import { SocketioService } from './services/socket/socketio.service';
import { ConnFailAlertComponent } from './components/conn-fail-alert/conn-fail-alert.component'
// import { NoopAnimationPlayer } from '@angular/animations'

// definindo como ptBr a linguagem do angular
registerLocaleData(ptBr)

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ListComponent,
    FilterComponent,
    ListHeaderComponent,
    FooterComponent,
    LoaderComponent,
    ConnFailAlertComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    NgbPaginationModule,
    NgbTooltipModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ToastNoAnimationModule
  ],
  providers: [
    ApiService,
    SocketioService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptorService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: LOCALE_ID, useValue: 'pt' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
