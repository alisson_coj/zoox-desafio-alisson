import { Component, OnInit, Output } from '@angular/core'
import { EventEmitter } from 'events'
import CitiesResponse from 'src/app/components/interfaces/cities-response'
import { City } from 'src/app/components/interfaces/city'
import SearchCity from 'src/app/components/interfaces/search-city'
import { State } from 'src/app/components/interfaces/state'
import { ApiService } from 'src/app/services/api.service'

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.sass']
})

export class FilterComponent implements OnInit {

  states: State[] = []
  cities: CitiesResponse = { data: [], meta: {} }
  search: SearchCity = { state: '', name: '', orderBy: 'asc' }
  loading: boolean = false

  constructor(private api: ApiService) { }

  async ngOnInit(): Promise<void> {
    await this.getStates()
  }

  private async getStates() {
    this.api.getAllStates()
      .subscribe((states: State[]) => this.states = states)
  }

  async getCities(page: number = 1) {
    // this.cities.data = [] // clear cities
    this.loading = true // open loading

    this.search.page = page // set page

    this.api.searchCity(this.search)
      .subscribe((cities: CitiesResponse) => {
        this.cities = cities
      })
      .add(() => {
        this.loading = false // end loading
      })
  }
}
