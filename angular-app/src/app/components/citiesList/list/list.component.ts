import { Component, Input, OnInit } from '@angular/core'
import CitiesResponse from 'src/app/components/interfaces/cities-response'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  @Input() cities: CitiesResponse

  constructor() { }

  ngOnInit(): void {
  }

}
