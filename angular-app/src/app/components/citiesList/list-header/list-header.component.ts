import { Component, Input, OnInit, Output } from '@angular/core'
import { EventEmitter } from '@angular/core'
import CitiesResponse from 'src/app/components/interfaces/cities-response'

@Component({
  selector: 'app-list-header',
  templateUrl: './list-header.component.html',
  styleUrls: ['./list-header.component.sass']
})
export class ListHeaderComponent implements OnInit {

  @Input() data: { perPage: number, total: number, pages: number }
  @Output() exec: EventEmitter<number> = new EventEmitter<number>()

  page: number = 1

  constructor() { }

  ngOnInit(): void {
  }

  pageChange(page: number) {
    this.exec.emit(page)
  }

}
