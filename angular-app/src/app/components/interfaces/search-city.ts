export default interface SearchCity {
  state?: string,
  name?: string,
  orderBy?: string,
  page?: number
}