export interface State {
  _id: string,
  name: string,
  abbr: string,
  createdAt: string,
  updatedAt: string
}
