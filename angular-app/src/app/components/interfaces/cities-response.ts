import { City } from "./city"

export default interface CitiesResponse {
  data: City[],
  meta: {
    total?: number,
    current_page?: number,
    pages?: number,
    perPage?: number
  }
}