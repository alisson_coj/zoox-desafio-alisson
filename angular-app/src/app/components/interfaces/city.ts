import { State } from "./state"

export interface City {
  _id: string,
  name: string,
  state: State,
  createdAt: string,
  updatedAt: string
}
