import { Component, OnInit } from '@angular/core'
import { SocketioService } from 'src/app/services/socket/socketio.service'

@Component({
  selector: 'app-conn-fail-alert',
  templateUrl: './conn-fail-alert.component.html',
  styleUrls: ['./conn-fail-alert.component.sass']
})
export class ConnFailAlertComponent implements OnInit {
  show: boolean = false
  msg: string = 'A aplicação está desconectada do servidor'

  constructor(private socketService: SocketioService) { }

  ngOnInit() {
    this.socketService.
      setupSocketConnection()
      .on('connect', (data) => {
        this.show = false
        console.info(`[Servidor] online`)
      })
      .on('disconnect', (data) => {
        this.show = true
        console.error(`[Servidor] ${this.msg}`)
      })
  }

}
