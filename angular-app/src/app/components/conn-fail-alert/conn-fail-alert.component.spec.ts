import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnFailAlertComponent } from './conn-fail-alert.component';

describe('ConnFailAlertComponent', () => {
  let component: ConnFailAlertComponent;
  let fixture: ComponentFixture<ConnFailAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnFailAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnFailAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
