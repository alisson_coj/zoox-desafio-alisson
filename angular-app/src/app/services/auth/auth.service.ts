import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, Subscription } from 'rxjs'
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private API_URL = `${environment.apiUrl}/api`
  private TOKEN_NAME = 'Zoox-auth-token'

  constructor(protected http: HttpClient) { }

  /**
   * return all states
   */
  login(): Subscription {
    return this.http
      .get(`${this.API_URL}/auth`)
      .subscribe((res: { token: string }) => {
        return localStorage.setItem(this.TOKEN_NAME, res.token)
      })
  }

  /**
   * get token from local storage
   */
  public getToken(): string {
    return localStorage.getItem(this.TOKEN_NAME)
  }
}