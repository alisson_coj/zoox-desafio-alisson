import { HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse, HttpInterceptor } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { Injectable } from '@angular/core'
import { tap, catchError } from "rxjs/operators"
import { ToastrService } from 'ngx-toastr'

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
    constructor(private toastr: ToastrService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            tap((evt) => {
                if (evt instanceof HttpResponse) {
                    if (evt.status === 200) {
                        if (evt.body) {
                            if (evt.body.data && !evt.body.data.length) {
                                this.toastr.info('nenhum dado encontrado', 'Pesquisa')
                            }
                        }
                    } else if (evt.status === 204) {
                        this.toastr.warning('nenhum dado encontrado', 'Pesquisa')
                    }
                }
            }),
            catchError((err: any) => {
                this.toastr.warning('O sistema está offline', 'Offline')
                return of(err)
            }))
    }
}