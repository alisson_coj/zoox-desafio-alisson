import { Injectable } from '@angular/core'
import * as io from 'socket.io-client'
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class SocketioService {
  // socket: SocketIOClient.Socket
  constructor() { }

  setupSocketConnection() {
    return io(environment.apiUrl)
  }
}
