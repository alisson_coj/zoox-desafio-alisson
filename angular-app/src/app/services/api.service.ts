import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment'
import SearchCity from '../components/interfaces/search-city'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private API_URL = `${environment.apiUrl}/api`

  constructor(protected http: HttpClient) { }

  /**
   * return all states
   */
  getAllStates(): Observable<any> {
    return this.http
      .get(`${this.API_URL}/states`)
  }

  /**
   * search a city bi a text
   */
  searchCity({ state, name, orderBy, page }: SearchCity): Observable<any> {
    return this.http
      .get(`${this.API_URL}/cities`, { params: { state, name, orderBy, page: `${page}` } })
  }
}
