# Desafio Zoox - descritivos

## Requisitos do projeto

| Local | Requisito              | Atendido | Observação |
|-------|-----------             |----------|------------|
| API   | API em Node            | Sim      |            |
| BD    | BD em MongoDB          | Sim      |            |
| API   | in/out em JSON         | Sim      |            |
| API   | CRUD cidades e estados | Sim      |            |
| FRONT | Consulta               | Sim      | Como a descrição do teste disse que seria **"uma tela de consulta"**, implementei a mesma, sem as funcionalidades de cadastro, atualização ou exclusão de registros. |
| Geral | Codificação UFT-8      | Sim      | Default do express com JSON. |
| Geral | Timezone brasília      | Sim      | Como não foi especificado se era na API ou na aplicação front, deixei armazenando como UTC no banco e no front como Brasília.  |
| Geral | Lingua PT-Br           | Sim      | Como conversado com o Victor, a orientação é para ser aplicado apenas ao front da aplicação (onde há interação com os usuários). |

## Estrutura Back-end
CRUD de cidades e estados, com opções de filtro e ordenação nos métodos de GET.

### Local
```bash
angular-app/
```

### Tecnologias usadas

| Tecnologia             | Descrição                     |
|------------------------|-------------------------------|
| Express                | base da aplicação             |
| Mongodb                | Banco de dados                |
| Redis                  | Cache                         |
| Joi                    | Validação de dados de entrada, prevenção de ataques ReDoS |
| Jest                   | automação de testes           |
| Socket.io              | no front e back para notificação de api online/offline |
| express-status-monitor | monitor de saúde da aplicação |


> ### Observações importantes
> Criei a API de uma forma um pouco verbosa para um projeto tão pequeno, mas, com os arquivos e responsabilidades bem divididas.
Coloquei cache com redis apenas na consulta de cidades (GET /api/states), caso a consulta não tenha sido feita com filtragem, apenas para mostrar o recurso de cache. Poderia ter aplicado o cache também na consulta de cidades, mas esta também sendo necessário armazenar em cache os filtros aplicados, já que cada um pode executar uma configuração de filtro diferente ao requisitar a rota.

---

## Estrutura Front-end
Visualização/consulta das cidades. Possui um filtro para pesquisar as cidades pelo nome, estado, ou até mesmo ordenar de forma  ascendente ou descendente. Como na descrição do teste falava apenas em consulta de dados no front, não fiz o tratamento de criação, edição e exclusão de dados.
Ao executar, se parecerá com o seguinte:

### Local
```bash
api/
```

### Tecnologias usadas

| Tecnologia   | Descrição                     |
|------------  |-------------------------------|
| Angular      | Framework base                |
| Bootstrap    | Framework CSS                 |
| Ng-bootstrap | Facilities com o bootstrap    |
| Ngx-toastr   | Balões de notificação         |
| Animate.css  | recursos visuais de animação  |
| Fontawesome  | recursos de ícones            |
| Socket.io    | no front e back para notificação de api online/offline |

> ### Observações importantes
> Criei 3 interceptors principais:
> - Um para mostrar um loader de carregamento em todas as requisições, a fim de mostrar ao usuário uma interação de espera. Como a execução está bem rápida, você não o verá com tanta facilidade
> - Uma para tratar erros vindos da API e mostrar ao usuário via toastr (balões de notificação)
> - Um para sempre enviar o token de autenticação no header de todas as requisições

-----

## Executando o projeto

> todos os projetos foram desenvolvidos com o *yarn* na versão *1.22.10* e o *node* na versão *14.15.3*

## Base

Primeiro, precisamos ter executando o **mongodb** e **redis**, e usei para isso o **docker-compose** para orquestrar as instâncias dos mesmos. Para executar, você precisará ter instalado os seguintes recursos:

- makefile
- docker
- docker-compose

Com esses recursos instalados, na pasta raiz do repositório, execute:

```bash
$ cd api/
$ make up
```

  > Este comando levantará a os containers considerando as variáveis de ambiente para conexão dos serviços, que estão no arquivo .*env* (não removi o *.env* do git para facilitar a execução)

---

## Back-end

na pasta raiz do repositório, execute o seguintes comandos:

``` bash
$ cd api/
$ yarn
$ yarn dev
```

> O node levantará a API na rota http://localhost:3333
> 
> Ao executar em modo de dev (variável NODE_ENV=development no arquivo *.env*), o script da API vai popular o banco de dados com uma carga de cidades e estados brasileiros (para fins de teste)

A API possui 4 modelos de rota principais:

| Métodos | Rota |
|---------| -----|
| **[GET, POST]**          | /api/cities |
| **[GET, PATCH, DELETE]** | /api/cities/:id |
| **[GET, POST]**          | /api/states |
| **[GET, PATCH, DELETE]** | /api/states/:id |

Abaixo, segue uma collection de rotas de teste para Postman:

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/12ac48e13906b2303214)

### Rodando os testes automatizados

```bash
$ yarn test
```

> Durante a execução dos testes, o mesmo apresentou alguns erros de memória com a instancia do mongodb que o teste levanta em memória. Os testes costumam funcionar todos com o cache limpo, mas, quando dá algum erro, os erros que apresentam apenas validam o próprio funcionamento correto da aplicação. Merece um bate papo para explicar.

### Autenticação de requisições

Apliquei uma rota na API chamada /auth, que me devolve um token md5, e no front passo esse token no header como *X-Api-Key*, e na API valido se o token foi informado. Fiz apenas para representar a troca dessas informações entre as aplicações, ou seja, sem um controle de login efetivo com algum regenciar de token como JWT.

### Verificação de conexão com Socket.io

Apliquei um módulo de socket.io no front e na api para o front conseguir monitorar quando a api está online ou off, orientando ao usuário verificar a conexão com a internet:

![](./readme-images/server-disconnected.png)

### Monitor de saúde da aplicação

Apliquei um módulo para gerar um monitor de saúde da aplicação Node. Basta acessar a rota http://localhost:3333/status e você deverá ver a seguinte página:

![](./readme-images/api-status-health.png)

---

## Front-end

Na pasta raiz do repositório, execute:

```bash
$ cd api/
$ yarn
$ ng serve
```

> O node levantará a API na rota http://localhost:4200

Ao acessar via navegador, você deverá ver uma página como a seguinte:

![](./readme-images/app-front-home.png)

Ao clicar em buscar, você verá uma amostragem de informações paginadas:

![](./readme-images/app-front-results.png)
