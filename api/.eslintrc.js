module.exports = {
  env: {
    node: true,
    es2021: true,
  },
  extends: [
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
    'prettier/@typescript-eslint',
    'standard'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module'
  },
  plugins: ['prettier', '@typescript-eslint', 'simple-import-sort'],
  rules: {
    'prettier/prettier': 'error',
    'simple-import-sort/sort': 'warn'
  },
  'comma-dangle': ['error', 'always']
}
