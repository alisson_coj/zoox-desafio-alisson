import { NextFunction, Request, Response } from "express"
import * as fs from 'fs'

import CitySchema from "../database/schemas/City.schema"
import StateSchema from "../database/schemas/State.schema"
import State from "../interfaces/State.interface"

/**
 * States methods service
 * 
 * @method populate
 */
export default class PopulateService {

  /**
   * Populate database with backup of states and cities
   * @param req 
   * @param res 
   * @param next 
   */
  static async populate() {
    try {
      // get json
      fs.readFile('data_mock/estados-cidades.json', 'utf-8', (err, data) => {
        if (err) {
          throw err
        }

        const json: [] = JSON.parse(data)
        json.forEach(async (state: { sigla: string, nome: string, cidades: [] }) => {
          const new_state: State = await StateSchema.create({
            name: state.nome,
            abbr: state.sigla
          })

          state.cidades.forEach(async (city: string) => {
            await CitySchema.create({
              name: city,
              state: new_state
            })
          })
        })
      })
    } catch (error) {
      return error
    }
  }
}
