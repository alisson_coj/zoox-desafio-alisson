
import State from '../interfaces/State.interface'
import StateSchema from '../database/schemas/State.schema'
import ErrorHandler from '../helpers/ErrorHandler'
import cache from '../database/cache/redisClient'
import RedisClient from '../database/cache/redisClient'
import { Response } from 'express'


/**
 * States methods service
 * 
 * @method search
 * @method getOne
 * @method create
 * @method update
 * @method delete
 */
export default class StateService {

  /**
   * get filtered states
   * @param text data to search
   */
  static async search(params: any, res: Response): Promise<State[]> {
    let result

    let { text, orderBy } = params // get params

    orderBy = orderBy || 'asc' // define a default order

    if (!text) {
      /**
       * set redis client
       * try yo get data from redis
       */
      let cacheClient = new RedisClient()

      const content = await cacheClient.get('states')

      if (content) {
        res.json(JSON.parse(`${content}`))
        return
      }

      /**
       * if not register on redis
       * get from database
       * after, register on redis
       */
      result = await StateSchema.find().sort({ name: orderBy }).lean().exec()
      let save_cache_result = cacheClient.set('states', result)

      res.json(result)
      return
    } else {
      result = await StateSchema
        .find()
        .sort({ name: orderBy })
        .or([
          { abbr: text },
          { name: text }
        ])
        .lean()
    }

    return result
  }

  /**
   * get specific state
   * @param _id state id
   */
  static async getOne(_id: String): Promise<State> {
    return await StateSchema.findOne({ _id }).lean()
  }

  /**
   * create a new state
   * @param params data to insert
   */
  static async create(params: State): Promise<State | any> {
    const { abbr, name } = params
    const state: State[] = await StateSchema.find({})
      .or([
        { abbr },
        { name }
      ])

    if (state.length) {
      const err: ErrorHandler = ErrorHandler.conflict()
      return Promise.reject(err)
    }

    const new_state = new StateSchema(params)
    return await new_state.save()
  }

  /**
   * update a state
   * @param params new data of state
   * @param _id state id
   */
  static async update(params: State, _id: String): Promise<State> {
    return await StateSchema.findOneAndUpdate({ _id }, params)
  }

  /**
   * delete a state
   * @param _id state id 
   */
  static async delete(_id: String): Promise<void> {
    return await StateSchema.deleteOne({ _id })
  }
}
