import City from '../interfaces/City.interface'
import CitySchema from '../database/schemas/City.schema'
import ErrorHandler from '../helpers/ErrorHandler'
import { Response } from 'express'

/**
 * Cities methods service
 * 
 * @method search
 * @method getOne
 * @method create
 * @method update
 * @method delete
 */
export default class CityService {

  /**
   * get cities filtered
   * @param params data to search
   */
  static async search(params: any, res: Response): Promise<any> {
    // define pagination variables
    const perPage = 10, page = params.page || 1

    // explode query params variables
    let { name, state, orderBy } = params

    // mount filter query
    let filterQuery: any = { name: { $regex: new RegExp(name), $options: 'i' } }

    // case has state filter
    if (state) {
      filterQuery.state = state
    }

    // execute query
    return CitySchema
      .find(filterQuery)
      .populate('state')
      .limit(perPage)
      .skip(perPage * (page - 1))
      .sort({ name: orderBy || 'asc' })
      .exec((err, cities) => {
        CitySchema.find(filterQuery).countDocuments().exec((err, count) => {
          res.json({
            data: cities,
            meta: {
              total: count,
              current_page: + page,
              pages: Math.ceil(count / perPage),
              perPage
            }
          })
        })
      })
  }

  /**
   * get specific city
   * @param _id city id
   */
  static async getOne(_id: String): Promise<City> {
    return await CitySchema.findOne({ _id })
  }

  /**
   * create a new city
   * @param params data to insert
   */
  static async create(params: City): Promise<City | void> {
    const { state, name } = params
    const city: City[] = await CitySchema.find({})
      .and([
        { state },
        { name }
      ])

    if (city.length) {
      const err: ErrorHandler = ErrorHandler.conflict('Já existe uma cidade com esse nome para esse estado')

      return Promise.reject(err)
    }

    const new_city = new CitySchema(params)

    return await new_city.save()
  }

  /**
   * update a city
   * @param params new data of city
   * @param _id city id
   */
  static async update(params: City, _id: String): Promise<City> {
    return await CitySchema.findOneAndUpdate({ _id }, params)
  }

  /**
   * delete a city
   * @param _id city id
   */
  static async delete(_id: String): Promise<void> {
    await CitySchema.deleteOne({ _id })
  }
}
