import express from 'express'
import cors from 'cors'
import dotenv from 'dotenv'
import statusMonitor from 'express-status-monitor'

import swaggerUi from 'swagger-ui-express'
import swaggerJsdoc from 'swagger-jsdoc'

import routes from './routes/index'
import responseError from './middlewares/response_error.middleware'
import doc from './doc'
import PopulateService from './services/populate.service'
import StateSchema from './database/schemas/State.schema'
import CitySchema from './database/schemas/City.schema'

export default class App {
  public express: express.Application

  public constructor(private database: Function) {
    this.express = express()

    this.express.use(express.json())
    this.express.use(cors())
    this.express.use(statusMonitor({
      title: 'Zoox - Buscador de cidades (API)'
    }))

    dotenv.config()

    this.setDoc()

    this.database()

    this.express.use(routes)
    this.middlewares()

    /**
     * case it is an dev ambient
     * populate database
     */
    if (process.env.NODE_ENV === 'development') {
      this.populateDatabase()
    }
  }

  private middlewares(): void {
    this.express.use(responseError)
  }

  private setDoc(): void {
    const swagger = swaggerJsdoc(doc)

    this.express.get('/api-doc/', swaggerUi.serve, swaggerUi.setup(swagger))
  }

  /**
   * clear and populate database with fake data
   */
  private async populateDatabase() {
    await StateSchema.deleteMany({})
    await CitySchema.deleteMany({})
    await PopulateService.populate()
  }
}
