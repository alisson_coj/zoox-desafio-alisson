import { Schema, model, Document } from 'mongoose'
import StateInterface from '../../interfaces/State.interface'

type State = Document & StateInterface

const StateSchema = new Schema({
  name: {
    type: String,
    required: [true, "'name' é um campo obrigatório"],
    unique: true,
    lowercase: true
  },
  abbr: {
    type: String,
    required: [true, "'abbr' é um campo obrigatório"],
    unique: true,
    uppercase: true,
    maxlength: [2, 'o campo precisa ter exatamente 2 caracteres'],
    minlength: [2, 'o campo precisa ter exatamente 2 caracteres']
  }
}, {
  timestamps: true
})

export default model<State>('states', StateSchema, 'states')
