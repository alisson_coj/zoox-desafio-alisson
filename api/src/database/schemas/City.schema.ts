import { Schema, model, Document } from 'mongoose'
import CityInterface from '../../interfaces/City.interface'

type City = Document & CityInterface

const CitySchema = new Schema({
  name: {
    type: String,
    required: [true, "'name' é um campo obrigatório"],
    unique: false
  },
  state: {
    type: Schema.Types.ObjectId,
    ref: 'states',
    unique: false
  }
}, {
  timestamps: true
})

export default model<City>('cities', CitySchema, 'cities')
