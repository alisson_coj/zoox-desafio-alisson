import mongoose from "mongoose"

export default () => {
  const { DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME } = process.env
  const url = `mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`

  mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  })

  mongoose.connection.on('error', () => console.log('db error'))
}