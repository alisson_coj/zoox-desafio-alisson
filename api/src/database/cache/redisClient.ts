import mongoose from 'mongoose'
import redis from 'redis'
import { promisify } from 'util'

export default class RedisClient {
  private client: redis.RedisClient
  constructor() {
    this.client = redis.createClient({
      host: process.env.REDIS_HOST,
      port: +process.env.REDIS_PORT,
      auth_pass: process.env.REDIS_PASS
    })

    this.client.get = promisify(this.client.get).bind(this.client)
    this.client.set = promisify(this.client.set).bind(this.client)
  }

  async get(schema: string) {
    return this.client.get(schema)
  }

  async set(schema: string, data: any) {
    return this.client.set(schema, JSON.stringify(data), 'EX', 60)
  }
}