import App from './app'
import database from './database/connection'

const port = process.env.PORT || 3333

const app = new App(database).express
const http = require('http').createServer(app)

const io = require('socket.io')(http, {
  cors: {
    origins: [process.env.FRONT_END_BASE_URL]
  }
})

io.on('connection', (socket: any) => {
  // ...
  socket.on('disconnect', () => {
    // ...
  })
})

http.listen(port, () => {
  console.log(`O pai está ON na porta ${port}`)
})
