import request from 'supertest'

import App from '../../app'
import State from '../../interfaces/State.interface'
import StateSchema from '../../database/schemas/State.schema'
import { generateFakeStates } from '../states.utils'
import { clearTestDb, closeTestDb, startTestDb } from '../setup'

process.env.NODE_ENV = 'test'
const app = new App(startTestDb).express


describe('Estados', () => {
  // -------------------------------------------------------------
  // database conn

  // start conn
  beforeAll(async (done) => {
    await startTestDb()
    await done()
  })

  beforeEach(async (done) => {
    // await StateSchema.deleteMany()
    await clearTestDb()
    await done()
  })

  // clear
  afterAll(async (done) => {
    await closeTestDb()
    await done()
  })

  // -------------------------------------------------------------
  it('GET /states - success', async (done) => {
    // clear state schema
    StateSchema.deleteMany({})

    // fake data
    const fake_states: State[] = generateFakeStates(3)

    // creating new states
    fake_states.forEach(async (state: State) => {
      await StateSchema.create(state)
    })

    await request(app).get(`/api/auth`)
      .then(async ({ body: { token } }) => {
        await request(app).get(`/api/states`)
          .set('x-api-key', token)
          .expect(200)
          .then(async res => {
            expect(Array.isArray(res.body)).toBeTruthy()
            expect(res.body.length > 0).toBeTruthy()

          })
      })

    await done()
  })

  it('POST /states - success', async (done) => {
    // pegando estado gerado, sem ser o fixo de teste
    const fake_state: State = generateFakeStates(0)[0]

    await request(app).get(`/api/auth`)
      .then(async ({ body: { token } }) => {
        await request(app).post(`/api/states`)
          .send(fake_state)
          .set('x-api-key', token)
          .expect(201)
          .then(async ({ body }) => {
            expect(body._id).toBeTruthy()
            expect(body.abbr).toBe(fake_state.abbr)
            expect(body.name).toBe(fake_state.name)
          })
      })

    await done()
  })

  it('POST /states - success', async (done) => {
    await request(app).get(`/api/auth`)
      .then(async ({ body: { token } }) => {
        await request(app).post(`/api/states`)
          .send({ name: "teste" })
          .set('x-api-key', token)
          .expect(422)
          .then(async ({ body }) => {
            expect(body.message).toBeTruthy()
          })
      })

    await done()
  })

  // it('POST /states - conflict', async (done) => {
  //   // pegando estado gerado, sem ser o fixo de teste
  //   const fake_state: State = generateFakeStates(0)[0]

  //   // criando estado
  //   const created_state: State = await StateSchema.create(fake_state)

  //   await request(app).post(`/api/states`)
  //     .send(fake_state)
  //     .then(async ({ body }) => {
  //       expect(body.message).toBeTruthy()

  //     })

  //   await done()
  // })

  it('PATCH /states/:id - success', async (done) => {
    // pegando estado gerado, sem ser o fixo de teste
    const fake_state: State = generateFakeStates(0)[0]

    // criando estado
    const created_state: State = await StateSchema.create(fake_state)

    created_state.name = 'aaaaaa'

    await request(app).get(`/api/auth`)
      .then(async ({ body: { token } }) => {
        await request(app).patch(`/api/states/${created_state._id}`)
          .send({
            name: created_state.name,
            abbr: created_state.abbr
          })
          .set('x-api-key', token)
          .expect(200)
          .then(async ({ body }) => {
            expect(body._id).toBeTruthy()

            const registered_state: State = await StateSchema.findOne({ _id: created_state._id })

            expect(registered_state).toBeTruthy()
            expect(registered_state.name).toBe(created_state.name)
            expect(registered_state.abbr).toBe(created_state.abbr)
          })
      })

    await done()
  })

  it('DELETE /states/:id - success', async (done) => {
    // pegando estado gerado, sem ser o fixo de teste
    const fake_state: State = generateFakeStates(0)[0]

    // criando estado
    const created_state: State = await StateSchema.create(fake_state)

    await request(app).get(`/api/auth`)
      .then(async ({ body: { token } }) => {
        await request(app).delete(`/api/states/${created_state._id}`)
          .set('x-api-key', token)
          .expect(200)
          .then(async ({ body }) => {
            expect(body).toEqual({ message: 'deleted' })
          })
      })

    await done()
  })
})
