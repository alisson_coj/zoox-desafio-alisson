import request from 'supertest'

import App from '../../app'
import State from '../../interfaces/State.interface'
import CitySchema from '../../database/schemas/City.schema'
import { clearTestDb, closeTestDb, startTestDb } from '../setup'
import { generateFakeCities, insertStateOnDb } from '../cities.utils'
import City from '../../interfaces/City.interface'
import StateSchema from '../../database/schemas/State.schema'

process.env.NODE_ENV = 'test'
const app = new App(startTestDb).express


describe('Cidades', () => {
  // -------------------------------------------------------------
  // database conn

  // start conn
  beforeAll(async (done) => {
    await startTestDb()
    await done()
  })

  afterEach(async (done) => {
    // await CitySchema.deleteMany()
    await clearTestDb()
    await done()
  })

  // clear
  afterAll(async (done) => {
    await closeTestDb()
    await done()
  })

  // -------------------------------------------------------------
  it('GET /cities - success', async (done) => {

    /**
     * creating fake data
     * get first state of db
     * or, create a new state
     */
    let state: State = await StateSchema.findOne({})

    if (!state) {
      state = await insertStateOnDb()
    }

    const fake_cities: City[] = await generateFakeCities(3, state)

    // criando cidades
    fake_cities.forEach(async (city: City) => {
      await CitySchema.create(city)
    })

    await request(app).get(`/api/auth`)
      .then(async ({ body: { token } }) => {
        await request(app).get(`/api/cities`)
          .set('x-api-key', token)
          .expect(200)
          .then(async res => {
            expect(Array.isArray(res.body.data)).toBeTruthy()
            expect(res.body.data.length > 0).toBeTruthy()
          })
      })

    await done()
  })

  it('POST /cities - success', async (done) => {
    // dados fake
    const state: State = await insertStateOnDb()
    const fake_city: City = generateFakeCities(1, state)[0]

    await request(app).get(`/api/auth`)
      .then(async ({ body: { token } }) => {
        await request(app).post(`/api/cities`)
          .send(fake_city)
          .set('x-api-key', token)
          .expect(201)
          .then(async ({ body }) => {
            expect(body._id).toBeTruthy()
            expect(body.state).toStrictEqual(`${fake_city.state._id}`)
            expect(body.name).toEqual(fake_city.name)
          })
      })

    await done()
  })

  it('POST /cities - conflict', async (done) => {
    // dados fake
    const state: State = await insertStateOnDb()
    const fake_city: City = generateFakeCities(1, state)[0]

    // criando cidades
    const created_city: City = await CitySchema.create(fake_city)

    await request(app).get(`/api/auth`)
      .then(async ({ body: { token } }) => {
        await request(app).post(`/api/cities`)
          .send(fake_city)
          .set('x-api-key', token)
          .then(async ({ body }) => {
            expect(body).toBeTruthy()
          })
      })

    await done()
  })

  it('PATCH /cities/:id - success', async (done) => {
    // dados fake
    const state: State = await insertStateOnDb()
    const fake_city: City = generateFakeCities(1, state)[0]

    // criando cidades
    const created_city: City = await CitySchema.create(fake_city)

    created_city.name = 'aaaaaa'

    await request(app).get(`/api/auth`)
      .then(async ({ body: { token } }) => {
        await request(app).patch(`/api/cities/${created_city._id}`)
          .send({ name: created_city.name })
          .set('x-api-key', token)
          .expect(200)
          .then(async ({ body, status }) => {
            expect(body._id).toBeTruthy()

            const registered_city: City = await CitySchema.findOne({ _id: created_city._id })
            expect(registered_city).toBeTruthy()
            expect(registered_city.name).toEqual(created_city.name)
            expect(registered_city.state).toStrictEqual(created_city.state._id)
          })
      })

    await done()
  })

  it('DELETE /cities/:id - success', async (done) => {
    // dados fake
    const state: State = await insertStateOnDb()
    const fake_city: City = generateFakeCities(1, state)[0]

    // criando cidades
    const created_city: City = await CitySchema.create(fake_city)

    await request(app).get(`/api/auth`)
      .then(async ({ body: { token } }) => {
        await request(app).delete(`/api/cities/${created_city._id}`)
          .send(created_city)
          .set('x-api-key', token)
          .expect(200)
          .then(async ({ body }) => {
            expect(body).toEqual({ message: 'deleted' })
          })
      })

    await done()
  })
})
