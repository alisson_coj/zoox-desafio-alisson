import faker from 'faker'

import City from '../interfaces/City.interface'
import State from '../interfaces/State.interface'
import StateSchema from '../database/schemas/State.schema'

export const CITY_TO_SEARCH = 'ES'

/**
 * Função útil para geração de Estados fake
 * @type {number} qtd - quantidade de estados a ser gerada
 */
export function generateFakeCities(qtd: number, state: State): City[] {
  let cities: City[] = []

  // analisa a qtd solicitada e cria de acordo
  for (let index = 0; index < qtd; index++) {
    let fake: City = { name: faker.address.city(), state }

    cities.push(fake)
  }

  return cities
}

export async function insertStateOnDb(): Promise<State> {
  return await StateSchema.create({ name: 'estado fake', abbr: 'ES' })
}