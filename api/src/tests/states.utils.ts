import faker from 'faker'

import State from '../interfaces/State.interface'

export const STATE_TO_SEARCH = 'ES'

/**
 * Função útil para geração de Estados fake
 * @type {number} qtd - quantidade de estados a ser gerada
 */
export function generateFakeStates(qtd: number): State[] {
  let states: State[] = [{ name: 'teste', abbr: 'ES' }]

  let last_generated

  // analisa a qtd solicitada e cria de acordo
  for (let index = 0; index < qtd; index++) {
    let fake = generate()

    // garantindo que não vá gerar estados duplicados
    while (
      last_generated &&
      (last_generated.name === fake.name || last_generated.abbr === fake.abbr)
    ) {
      fake = generate()
    }


    // guarda no array de gerados
    states.push(fake)

    // armazena o último gerado
    last_generated = fake
  }

  return states
}

function generate(): State {
  return {
    name: faker.address.state(),
    abbr: faker.address.stateAbbr()
  }
}