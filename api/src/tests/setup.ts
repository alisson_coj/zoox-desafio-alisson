import mongoose from 'mongoose'
// import connection from '../database/connection'

export async function startTestDb(): Promise<void> {
  // verifica se possui a valiável de ambiente ideal para rodar o ambiente de teste
  if (!process.env.MONGO_URL) {
    throw new Error('Variável de ambiente para o MongoDB simulado não instanciada, pode ser problema no jest-mongodb')
  }

  // Executa a conexão
  await mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
  })
    .catch((err) => console.log(err))
    .then(() => clearTestDb())
}

export async function closeTestDb(): Promise<void> {
  await mongoose.disconnect()
}

export async function clearTestDb() {
  mongoose.connection.dropDatabase()
  const collections = Object.keys(mongoose.connection.collections)
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName]
    await collection.deleteMany({})
  }
}
