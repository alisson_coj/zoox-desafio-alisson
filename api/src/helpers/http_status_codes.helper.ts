export const HTTP_STATUS_ERROR = 500
export const HTTP_STATUS_SUCCESS = 200
export const HTTP_STATUS_CREATED = 201
export const HTTP_STATUS_NOT_FOUND = 404
export const HTTP_STATUS_UNAUTHORIZED = 401
export const HTTP_STATUS_BAD_REQUEST = 400
export const HTTP_STATUS_CONFLICT = 409
