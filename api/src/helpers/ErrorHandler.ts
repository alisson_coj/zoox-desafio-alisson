import { Response } from 'express'
import {
  HTTP_STATUS_BAD_REQUEST,
  HTTP_STATUS_ERROR,
  HTTP_STATUS_CONFLICT,
  HTTP_STATUS_UNAUTHORIZED,
  HTTP_STATUS_NOT_FOUND
} from './http_status_codes.helper'

export default class ErrorHandler extends Error {
  status: number

  constructor(status: number, message: string) {
    super()
    this.status = status
    this.message = message
  }

  static badRequest(message: string): ErrorHandler {
    return new ErrorHandler(HTTP_STATUS_BAD_REQUEST, message)
  }

  static internal(message: string): ErrorHandler {
    return new ErrorHandler(HTTP_STATUS_ERROR, message)
  }

  static conflict(message?: string): ErrorHandler {
    return new ErrorHandler(HTTP_STATUS_CONFLICT, message || 'esse registro já existe no banco')
  }

  static auth(message: string): ErrorHandler {
    return new ErrorHandler(HTTP_STATUS_UNAUTHORIZED, message)
  }

  static notFound(message: string): ErrorHandler {
    return new ErrorHandler(HTTP_STATUS_NOT_FOUND, message)
  }
}
