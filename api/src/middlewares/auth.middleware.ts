import { NextFunction, Response, Request } from "express"
import ErrorHandler from "../helpers/ErrorHandler"
import { tokenValidation } from '../routes/validators/auth.validator'

export default async (req: Request, res: Response, next: NextFunction) => {
  try {
    // return res.send(req.headers['x-api-key'])
    await tokenValidation.validateAsync(req.headers['x-api-key'])
    next()
  } catch (err) {
    err = ErrorHandler.auth(`auth fail: ${err.message}`)
    return next(err)
  }
}