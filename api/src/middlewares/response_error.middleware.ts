import Joi, { ValidationError } from "@hapi/joi"
import { NextFunction, Request, Response } from "express"
import ErrorHandler from "../helpers/ErrorHandler"

export default (err: ErrorHandler | Error, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof ErrorHandler) {
    const { status, message } = err
    res.status(status).json({ status, message })
  }

  if (err instanceof ValidationError) {
    return res.status(422).json({ status: 422, message: err.message })
  }

  return res.status(500).json({ status: 500, message: err.message })
}