export default {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Zoox Estados e Cidades API',
      version: '1.0.0',
      description:
        "Um CRUD simples para gerenciamento de cidades e estados",
      license: {
        name: "MIT",
        url: "https://spdx.org/licenses/MIT.html",
      },
      contact: {
        name: "Alisson",
        url: "https://www.linkedin.com/in/alissonjunior/",
        email: "alisson.coj@hotmail.com",
      },
    }
  },
  servers: [
    {
      url: "http://localhost:3333/api",
    },
  ],
  apis: ['./routes/city.route.ts', './routes/state.route.ts']
}