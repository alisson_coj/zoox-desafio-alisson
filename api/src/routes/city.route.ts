import { Router } from 'express'
import CityController from '../controllers/city.controller'

const router = Router()

router
  .route('/')

  /**
   * @swagger
   * 
   * /cities:
   *   get:
   *     sumary: Busca cidades
   *     description: Retorna uma lista de cidades buscadas por um filtro
   */
  .get(CityController.search)

  /**
   * @swagger
   * 
   * /cities:
   *  post:
   *    sumary: Cria cidades
   *    description: Cria novos registros de cidades
   *    requestBody:
   *      content:
   *        application/json:
   *          schema:
   *            properties:
   *              name:
   *                type: string
   *                description: nome da cidade
   *              state:
   *                type: State
   *                description: estado ao qual a cidade pertence
   *    responses:
   *      201:
   *        description: cidades criadas com sucesso
   */
  .post(CityController.create)


router
  .route('/:id')
  .get(CityController.getOne)
  .patch(CityController.update)
  .delete(CityController.delete)

export default router
