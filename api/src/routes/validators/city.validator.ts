import Joi from '@hapi/joi'

const alphaNum = Joi.string().alphanum()

export const cityId = alphaNum

export const cityCreation = Joi.object({
  name: Joi.string().required(),
  state: Joi.required()
})

export const cityUpdate = Joi.object({
  _id: alphaNum,
  name: Joi.string(),
  state: Joi.allow()
})

export const citySearch = Joi.object({
  name: Joi.string().allow(''),
  state: alphaNum.allow(''),
  page: alphaNum.allow(''),
  orderBy: alphaNum.valid('asc', 'desc')
})