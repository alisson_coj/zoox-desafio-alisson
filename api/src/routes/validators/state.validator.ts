import Joi from '@hapi/joi'

const alphaNum = Joi.string().alphanum()

export const stateId = alphaNum

export const stateCreation = Joi.object({
  name: Joi.string().required(),
  abbr: alphaNum.required().max(2).min(2)
})

export const stateUpdate = Joi.object({
  name: Joi.string().required(),
  abbr: alphaNum.required()
})