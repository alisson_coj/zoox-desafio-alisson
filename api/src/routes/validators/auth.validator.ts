import Joi from '@hapi/joi'

export const tokenValidation = Joi.string().alphanum().token().required()
