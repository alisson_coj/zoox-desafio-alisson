import express, { Request, Response } from 'express'
import PopulateService from '../services/populate.service'
import crypto from 'crypto'

import cityRouter from './city.route'
import stateRouter from './state.route'
import authMiddleware from '../middlewares/auth.middleware'

const base_url: string = '/api'
const router = express.Router()

router.get(`${base_url}`, (req: Request, res: Response) => {
  return res.json({ message: 'Hello!' })
})

/**
 * return a simple token to validate to simulate auth
 */
router.get(`${base_url}/auth`, (req: Request, res: Response) => {
  const token = crypto.createHash('md5').update('').digest('hex')
  res.json({ token })
})

router.use(`${base_url}/cities`, authMiddleware, cityRouter)
router.use(`${base_url}/states`, authMiddleware, stateRouter)

/**
 * not found in JSON
 */
router.get('*', (req: Request, res: Response) => {
  return res.status(404).json({ message: 'not found' })
})

export default router
