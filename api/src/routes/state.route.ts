import { Router } from 'express'
import StateController from '../controllers/state.controller'
import { stateCreation, stateUpdate } from './validators/state.validator'

const router = Router()

router
  .route('/')
  .get(StateController.search)
  .post(StateController.create)


router
  .route('/:id')
  .get(StateController.getOne)
  .patch(StateController.update)
  .delete(StateController.delete)

export default router
