import State from './State.interface'

export default interface City {
  _id?: String,
  name: String,
  state: State,
  created_at?: String,
  updated_at?: String
}
