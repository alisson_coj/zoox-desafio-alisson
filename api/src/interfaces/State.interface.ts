export default interface State {
  _id?: String,
  name: String,
  abbr: String,
  created_at?: String,
  updated_at?: String
}
