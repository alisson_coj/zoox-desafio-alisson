import { NextFunction, Request, Response } from 'express'
import City from '../interfaces/City.interface'

import CityService from '../services/city.service'
import { HTTP_STATUS_CREATED, HTTP_STATUS_SUCCESS } from '../helpers/http_status_codes.helper'
import { cityCreation, cityUpdate, citySearch, cityId } from '../routes/validators/city.validator'
import Joi from '@hapi/joi'
import ErrorHandler from '../helpers/ErrorHandler'
import { nextTick } from 'process'

export default class CityController {

  /**
   * get cities by text search
   */
  static async search({ query }: Request, res: Response, next: NextFunction) {
    try {
      await citySearch.validateAsync(query)
      await CityService.search(query, res)
    } catch (err) {
      next(err)
    }
  }

  /**
   * get one specific city by id
   * @param id
   * @param res 
   */
  static async getOne({ params: { id } }: Request, res: Response, next: NextFunction) {
    try {
      await cityId.validateAsync(id)

      const city: City = await CityService.getOne(id)

      res.status(HTTP_STATUS_SUCCESS).json(city)
    } catch (err) {
      next(err)
    }
  }

  /**
   * create new city
   * @param body
   * @param res 
   */
  static async create({ body }: Request, res: Response, next: NextFunction) {
    try {
      await cityCreation.validateAsync(body)

      const city = await CityService.create(body)

      return res.status(HTTP_STATUS_CREATED).json(city)
    } catch (err) {
      next(err)
    }
  }

  /**
   * update city
   * @param body
   * @param params
   * @param res 
   */
  static async update({ params, body }: Request, res: Response, next: NextFunction) {
    try {
      await cityUpdate.validateAsync(body)

      const { id } = params
      await cityId.validateAsync(id)

      const city: City = await CityService.update(body, id)

      res.status(HTTP_STATUS_SUCCESS).json(city)
    } catch (err) {
      next(err)
    }
  }

  /**
   * delete city by id
   * @param id city id
   * @param res 
   */
  static async delete({ params: { id } }: Request, res: Response, next: NextFunction) {
    try {
      await cityId.validateAsync(id)

      await CityService.delete(id)

      res.status(HTTP_STATUS_SUCCESS).json({ message: 'deleted' })
    } catch (err) {
      next(err)
    }

  }
}