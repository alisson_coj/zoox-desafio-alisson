import { Request, Response, NextFunction } from 'express'
import State from '../interfaces/State.interface'
import StateService from '../services/state.service'
import { HTTP_STATUS_CREATED, HTTP_STATUS_SUCCESS } from '../helpers/http_status_codes.helper'
import { stateCreation, stateUpdate } from '../routes/validators/state.validator'
import ErrorHandler from '../helpers/ErrorHandler'
import { stateId } from '../routes/validators/state.validator'

export default class StateController {
  /**
   * get states by text
   */
  static async search({ query }: Request, res: Response, next: NextFunction) {
    try {
      const states: State[] = await StateService.search(query, res)

      res.status(HTTP_STATUS_SUCCESS).json(states)
    } catch (err) {
      next(err)
    }
  }

  /**
   * get one specific state by id
   * @param id state id
   * @param res response
   */
  static async getOne({ params: { id } }: Request, res: Response, next: NextFunction) {
    try {
      await stateId.validateAsync(id)

      const state: State = await StateService.getOne(id)

      res.status(HTTP_STATUS_SUCCESS).json(state)
    } catch (err) {
      next(err)
    }
  }

  /**
   * create new state
   * @param body
   * @param res response
   */
  static async create({ body }: Request, res: Response, next: NextFunction) {
    try {
      const result = await stateCreation.validateAsync(body)

      const state = await StateService.create(body)

      return res.status(HTTP_STATUS_CREATED).json(state)
    } catch (err) {
      next(err)
    }
  }

  /**
   * update state
   * @param body state data
   * @param res response
   */
  static async update({ params, body }: Request, res: Response, next: NextFunction) {
    try {
      await stateUpdate.validateAsync(body)

      const { id } = params
      await stateId.validateAsync(id)
      const state: State = await StateService.update(body, id)

      res.status(HTTP_STATUS_SUCCESS).json(state)
    } catch (err) {
      next(err)
    }
  }

  /**
   * delete state by id
   * @param id state id
   * @param res response
   */
  static async delete({ params: { id } }: Request, res: Response, next: NextFunction) {
    try {
      await stateId.validateAsync(id)

      await StateService.delete(id)

      res.status(HTTP_STATUS_SUCCESS).json({ message: 'deleted' })
    } catch (err) {
      next(err)
    }
  }
}
